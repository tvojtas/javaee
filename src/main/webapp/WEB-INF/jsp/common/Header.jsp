<div class="header">
	<nav class="navbar navbar-inverse">
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="${pageContext.request.contextPath}/">HOME</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active">
				<li><a href="${pageContext.request.contextPath}/user/list">User list</a></li>
				<li><a href="${pageContext.request.contextPath}/user/cities">Cities</a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="${pageContext.request.contextPath}/auth/login"><span
						class="glyphicon glyphicon-log-in"> Login</span></a></li>
			</ul>
		</div>
	</nav>
</div>