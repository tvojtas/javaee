<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="User list">
	<jsp:attribute name="body">
	<a href="${pageContext.request.contextPath}/user/new" class="btn btn-primary">NEW USER</a>
	<a href="${pageContext.request.contextPath}/user/cities" class="btn btn-primary">SHOW USERS PER CITY</a>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>id</th>
				<th>name</th>
				<th>surname</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${users}" var="user">
			<tr>
				<td><c:out value="${user.id}" /></td>
				<td><c:out value="${user.name}" /></td>
				<td><c:out value="${user.surname}" /></td>
				<td><a href="${pageContext.request.contextPath}/user/meetings?id=${user.id}" class="btn btn-primary">meetings</a></td>
				<td><a href="${pageContext.request.contextPath}/user/detail?id=${user.id}" class="btn btn-primary">details</a></td>
				<td><a href="${pageContext.request.contextPath}/user/update?id=${user.id}" class="btn btn-primary">update</a></td>
				<td><a href="${pageContext.request.contextPath}/user/delete?id=${user.id}" class="btn btn-primary">delete</a></td>
			</tr>
		</c:forEach>	
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>
