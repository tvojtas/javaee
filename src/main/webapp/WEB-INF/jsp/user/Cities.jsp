<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="Cities">
	<jsp:attribute name="body">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>city</th>
				<th>user count</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${cities}" var="city">
			<tr>
				<td><c:out value="${city.cityName}" /></td>
				<td><c:out value="${city.userCount}" /></td>
			</tr>
		</c:forEach>	
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>