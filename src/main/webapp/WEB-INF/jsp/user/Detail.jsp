<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="User details">
	<jsp:attribute name="body">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>id</th>
				<th>name</th>
				<th>surname</th>
				<th>nickname</th>
				<th>email</th>
				<th>age</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><c:out value="${user.id}" /></td>
				<td><c:out value="${user.name}" /></td>
				<td><c:out value="${user.surname}" /></td>
				<td><c:out value="${user.nickname}" /></td>
				<td><c:out value="${user.email}" /></td>
				<td><c:out value="${user.age}" /></td>
				<td><a href="${pageContext.request.contextPath}/user/detail/contacts?id=${user.id}" class="btn btn-primary">contacts</a></td>
				<td><a href="${pageContext.request.contextPath}/user/detail/addresses?id=${user.id}" class="btn btn-primary">addresses</a></td>
			</tr>
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>
