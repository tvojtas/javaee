<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="User contacts">
	<jsp:attribute name="body">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>id</th>
				<th>name</th>
				<th>surname</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${contacts}" var="contact">
			<tr>
				<td><c:out value="${contact.id}" /></td>
				<td><c:out value="${contact.contact}" /></td>
				<td><c:out value="${contact.type}" /></td>
			</tr>
		</c:forEach>	
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>