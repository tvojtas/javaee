<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="New user">
	<jsp:attribute name="body">
		
		<form role="form" method="post">
		<fieldset>
    	<legend>ENTER NEW USER:</legend>
		 Email: <input type = "text" name = "email">
         <br/>
         Password: <input type = "password" name = "pwd">
         <br/>
         Nickname: <input type = "text" name = "nickname">
         <br/>
         First Name: <input type = "text" name = "name">
         <br/>
         Last name: <input type = "text" name = "surname">
         <br/>
         Birthday: <input type = "date" name = "birthday" />
         <br/>
         Age: <input type = "text" name = "age">
         <br/>
         <input type = "submit" value = "Submit" />
         </fieldset>
      	</form>
	</jsp:attribute>
</my:pagetemplate>