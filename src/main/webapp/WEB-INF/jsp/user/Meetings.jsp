<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="User meetings">
	<jsp:attribute name="body">
	<table class="table table-striped">
		USER HAS <c:out value="${userMeetings.count}" /> MEETINGS, NEXT MEETING : <c:out value="${userMeetings.next}" />
		<thead>
			<tr>
				<th>date</th>
				<th>place</th>
				<th>note</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${userMeetings.meetings}" var="meeting">
			<tr>
				<td><c:out value="${meeting.date}" /></td>
				<td><c:out value="${meeting.place}" /></td>
				<td><c:out value="${meeting.note}" /></td>

			</tr>
			</c:forEach>
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>
