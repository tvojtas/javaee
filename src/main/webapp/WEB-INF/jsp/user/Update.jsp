<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="Update user">
	<jsp:attribute name="body">
		
		<form role="form" method="post">
		<fieldset>
    	<legend>ENTER NEW USER INFO:</legend>
		 Email: <input type = "text" name = "email" value="${user.email}">
         <br/>
         Nickname: <input type = "text" name = "nickname" value="${user.nickname}">
         <br/>
         First Name: <input type = "text" name = "name" value="${user.name}">
         <br/>
         Last name: <input type = "text" name = "surname" value="${user.surname}">
         <br/>
         Birthday: <input type = "date" name = "birthday" value="${user.birthday}">
         <br/>
         Age: <input type = "text" name = "age" value="${user.age}">
         <br/>
         <input type = "submit" value = "Submit">
         </fieldset>
      	</form>
	</jsp:attribute>
</my:pagetemplate>