<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="User addresses">
	<jsp:attribute name="body">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>id</th>
				<th>city</th>
				<th>street</th>
				<th>house number</th>
				<th>zip code</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${addresses}" var="address">
			<tr>
				<td><c:out value="${address.id}" /></td>
				<td><c:out value="${address.city}" /></td>
				<td><c:out value="${address.street}" /></td>
				<td><c:out value="${address.houseNumber}" /></td>
				<td><c:out value="${address.zipCode}" /></td>
			</tr>
		</c:forEach>	
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>