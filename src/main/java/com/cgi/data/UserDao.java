package com.cgi.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import com.cgi.api.AddressViewDto;
import com.cgi.api.ContactViewDto;
import com.cgi.api.MeetingViewDto;
import com.cgi.api.NewUserDto;
import com.cgi.api.UpdateUserDto;
import com.cgi.api.UserDetailedViewDto;
import com.cgi.api.UserSimpleViewDto;
import com.cgi.api.UsersPerCityDto;

@Stateless
public class UserDao {
	
	@Resource(lookup = "jdbc/bootcampCGI")
	private DataSource dataSource;
	
	public void createUser() {
		
	}
	public UserDetailedViewDto findById(Long id) {
		String findByIdSQL = "SELECT  id_user, first_name, surname, nickname, email, age, birthday FROM user u WHERE u.id_user = ?";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(findByIdSQL);) {
			ps.setLong(1, id);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					UserDetailedViewDto user = new UserDetailedViewDto();
					user.setId(rs.getLong("id_user"));
					user.setSurname(rs.getString("surname"));
					user.setName(rs.getString("first_name"));
					user.setEmail(rs.getString("email"));
					user.setNickname(rs.getString("nickname"));
					user.setAge(rs.getInt("age"));
					user.setBirthday(rs.getDate("birthday"));
					return user;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public List<UserSimpleViewDto> findAll() {
		List<UserSimpleViewDto> users = new ArrayList<>();
		try (Connection connection = dataSource.getConnection()) {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT id_user, first_name, surname FROM user u");
			while (rs.next()) {
				UserSimpleViewDto user = new UserSimpleViewDto();
				user.setId(rs.getLong("id_user"));
				user.setSurname(rs.getString("surname"));
				user.setName(rs.getString("first_name"));
				users.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}
	public void updateUser(UpdateUserDto user, Long id) {
		String createNewUserSQL = "UPDATE user SET email = ?, nickname = ?, first_name = ?, surname = ?, birthday = ?, age = ? WHERE id_user=?";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(createNewUserSQL);) {
			ps.setString(1, user.getEmail());
			ps.setString(2, user.getNickname());
			ps.setString(3, user.getName());
			ps.setString(4, user.getSurname());
			ps.setDate(5, user.getBirthday());
			ps.setInt(6, user.getAge());
			ps.setLong(7, id);
			ps.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void deleteUser(Long id) {
		String deleteInUsersSQL = "DELETE FROM user WHERE user.id_user = ?";
		try (Connection connection = dataSource.getConnection(); PreparedStatement ps = connection.prepareStatement(deleteInUsersSQL);) {
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String deleteInMeetingsSQL = "DELETE FROM user_has_meeting WHERE user_has_meeting.id_user = ?";
		try (Connection connection = dataSource.getConnection(); PreparedStatement ps = connection.prepareStatement(deleteInMeetingsSQL);) {
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String deleteInRolesSQL = "DELETE FROM role_has_user WHERE role_has_user.id_user = ?";
		try (Connection connection = dataSource.getConnection(); PreparedStatement ps = connection.prepareStatement(deleteInRolesSQL);) {
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String deleteInContactSQL = "DELETE FROM contact WHERE contact.id_user = ?";
		try (Connection connection = dataSource.getConnection(); PreparedStatement ps = connection.prepareStatement(deleteInContactSQL);) {
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String deleteInRelationshipSQL = "DELETE FROM relationship WHERE relationship.id_user1 = ? OR relationship.id_user2 = ?";
		try (Connection connection = dataSource.getConnection(); PreparedStatement ps = connection.prepareStatement(deleteInRelationshipSQL);) {
			ps.setLong(1, id);
			ps.setLong(2, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public List<ContactViewDto> findContactsById(Long id) {
		String findContactsByIdSQL = "SELECT id_contact, contact, contact_type.title as type FROM contact LEFT JOIN contact_type ON contact.id_contact_type=contact_type.id_contact_type WHERE contact.id_user=?";
		List<ContactViewDto> contacts = new ArrayList<>();
		try (Connection connection = dataSource.getConnection(); PreparedStatement ps = connection.prepareStatement(findContactsByIdSQL);) {
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ContactViewDto contact = new ContactViewDto();
				contact.setId(rs.getLong("id_contact"));
				contact.setContact(rs.getString("contact"));
				contact.setType(rs.getString("type"));
				contacts.add(contact);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return contacts;
	}
	public List<AddressViewDto> findCAddressesById(Long id) {
		String findCAddressesByIdSQL = "SELECT address.id_address, city, street, house_number, zip_code FROM address LEFT JOIN user ON address.id_address=user.id_address WHERE id_user=?";
		List<AddressViewDto> addresses = new ArrayList<>();
		try (Connection connection = dataSource.getConnection(); PreparedStatement ps = connection.prepareStatement(findCAddressesByIdSQL);) {
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				AddressViewDto address = new AddressViewDto();
				address.setId(rs.getLong("id_address"));
				address.setCity(rs.getString("city"));
				address.setStreet(rs.getString("street"));
				address.setHouseNumber(rs.getInt("house_number"));
				address.setZipCode(rs.getString("zip_code"));
				addresses.add(address);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return addresses;
	}
	
	public void createNewUser(NewUserDto user) {
		String createNewUserSQL = "INSERT INTO user(email, password, nickname, first_name, surname, birthday, age, id_address) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(createNewUserSQL);) {
			ps.setString(1, user.getEmail());
			ps.setString(2, user.getPwd());
			ps.setString(3, user.getNickname());
			ps.setString(4, user.getName());
			ps.setString(5, user.getSurname());
			ps.setDate(6, user.getBirthday());
			ps.setInt(7, user.getAge());
			ps.setLong(8, 1L);
			ps.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public List<UsersPerCityDto> usersPerCity() {
		List<UsersPerCityDto> cities = new ArrayList<>();
		try (Connection connection = dataSource.getConnection()) {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT city, COUNT(user.id_user) AS count FROM user LEFT JOIN address ON user.id_address=address.id_address GROUP BY city");
			while (rs.next()) {
				UsersPerCityDto city = new UsersPerCityDto();
				city.setCityName(rs.getString("city"));
				city.setUserCount(rs.getInt("count"));
				cities.add(city);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return cities;
	}
	public List<MeetingViewDto> findMeetingsById(Long id) {
		List<MeetingViewDto> meetings = new ArrayList<>();
		String userMeetingsCountSQL = "SELECT meeting_datetime, place, note FROM user_has_meeting LEFT JOIN meeting ON user_has_meeting.id_meeting=meeting.id_meeting WHERE user_has_meeting.id_user = ?";
		try (Connection connection = dataSource.getConnection(); PreparedStatement ps = connection.prepareStatement(userMeetingsCountSQL);) {
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				MeetingViewDto meeting = new MeetingViewDto();
				meeting.setDate(rs.getTimestamp("meeting_datetime"));
				meeting.setPlace(rs.getString("place"));
				meeting.setNote(rs.getString("note"));
				meetings.add(meeting);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return meetings;
	}
	
	public Integer userMeetingsCount(Long id) {
		String userMeetingsCountSQL = "SELECT COUNT(id_user) AS count FROM user_has_meeting WHERE user_has_meeting.id_user = ?";
		try (Connection connection = dataSource.getConnection(); PreparedStatement ps = connection.prepareStatement(userMeetingsCountSQL);) {
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			rs.next();
			Integer count = rs.getInt("count");
			return count;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public Timestamp userNextMeeting(Long id) {
		String userNextMeetingSQL = "SELECT MIN(meeting_datetime) AS min FROM user_has_meeting LEFT JOIN meeting ON user_has_meeting.id_meeting=meeting.id_meeting WHERE user_has_meeting.id_user = ?";
		try (Connection connection = dataSource.getConnection(); PreparedStatement ps = connection.prepareStatement(userNextMeetingSQL);) {
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			rs.next();
			Timestamp date = rs.getTimestamp("min");
			return date;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
