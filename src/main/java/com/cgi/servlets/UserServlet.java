package com.cgi.servlets;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.AddressViewDto;
import com.cgi.api.ContactViewDto;
import com.cgi.api.UserDetailedViewDto;
import com.cgi.api.UserMeetingsDto;
import com.cgi.api.UserSimpleViewDto;
import com.cgi.api.UsersPerCityDto;
import com.cgi.service.UserService;

/**
 * Servlet implementation class UserServlet
 */

@WebServlet(name = "UserServlet", urlPatterns = {"/user/*"})
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private UserService userService;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String pathInfo = request.getPathInfo();
		if (pathInfo != null && pathInfo.contains("/detail")) {
			if (pathInfo.contains("/addresses")) {
				String idStr = request.getParameter("id");
				Long id = Long.parseLong(idStr);
				List<AddressViewDto> addresses = userService.findAddressessById(id);
				request.setAttribute("addresses", addresses);
				request.getRequestDispatcher("/WEB-INF/jsp/user/Addresses.jsp").forward(request, response);
			} else if (pathInfo.contains("/contacts")) {
				String idStr = request.getParameter("id");
				Long id = Long.parseLong(idStr);
				List<ContactViewDto> contacts = userService.findContactsById(id);
				request.setAttribute("contacts", contacts);
				request.getRequestDispatcher("/WEB-INF/jsp/user/Contacts.jsp").forward(request, response);
			} else if (pathInfo.contains("/meeting")) {
				String idStr = request.getParameter("id");
				Long id = Long.parseLong(idStr);
				List<ContactViewDto> contacts = userService.findContactsById(id);
				request.setAttribute("contacts", contacts);
				request.getRequestDispatcher("/WEB-INF/jsp/user/Contacts.jsp").forward(request, response);
			} else {
				String idStr = request.getParameter("id");
				Long id = Long.parseLong(idStr);
				UserDetailedViewDto user = userService.findById(id);
				request.setAttribute("user", user);
				request.getRequestDispatcher("/WEB-INF/jsp/user/Detail.jsp").forward(request, response);
			}
		} else if (pathInfo != null && pathInfo.endsWith("/list")) {
			List<UserSimpleViewDto> users = userService.findAll();
			request.setAttribute("users", users);
			request.getRequestDispatcher("/WEB-INF/jsp/user/List.jsp").forward(request, response);
		} else if (pathInfo != null && pathInfo.endsWith("/new")) {
			
			request.getRequestDispatcher("/WEB-INF/jsp/user/New.jsp").forward(request, response);
		} else if (pathInfo != null && pathInfo.contains("/update")) {
			String idStr = request.getParameter("id");
			Long id = Long.parseLong(idStr);
			UserDetailedViewDto user = userService.findById(id);
			request.setAttribute("user", user);
			request.getRequestDispatcher("/WEB-INF/jsp/user/Update.jsp").forward(request, response);
		} else if (pathInfo != null && pathInfo.contains("/meetings")) {
			String idStr = request.getParameter("id");
			Long id = Long.parseLong(idStr);
			UserMeetingsDto userMeetings = userService.findMeetingsById(id);
			request.setAttribute("userMeetings", userMeetings);
			request.getRequestDispatcher("/WEB-INF/jsp/user/Meetings.jsp").forward(request, response);
		} else if (pathInfo != null && pathInfo.contains("/cities")) {
			List<UsersPerCityDto> cities = userService.usersPerCity();
			request.setAttribute("cities", cities);
			request.getRequestDispatcher("/WEB-INF/jsp/user/Cities.jsp").forward(request, response);
		} else if (pathInfo != null && pathInfo.contains("/delete")) {
			String idStr = request.getParameter("id");
			Long id = Long.parseLong(idStr);
			userService.deleteUser(id);
			System.out.println("RUNNIN");
			response.sendRedirect(request.getContextPath()+"/user/list");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pathInfo = request.getPathInfo();
		if (pathInfo != null && pathInfo.contains("/new")) {
			String pwd = request.getParameter("pwd");
			String email = request.getParameter("email");
			String name = request.getParameter("name");
			String surname = request.getParameter("surname");
			String nickname = request.getParameter("nickname");
			Date birthday = Date.valueOf(request.getParameter("birthday"));
			Integer age = Integer.valueOf(request.getParameter("age"));
			userService.createNewUser(pwd, email, name, surname, nickname, birthday, age);
			response.sendRedirect(request.getContextPath()+"/user/list");
		} else if (pathInfo != null && pathInfo.contains("/update")) {
			String pwd = request.getParameter("pwd");
			String email = request.getParameter("email");
			String name = request.getParameter("name");
			String surname = request.getParameter("surname");
			String nickname = request.getParameter("nickname");
			Date birthday = Date.valueOf(request.getParameter("birthday"));
			Integer age = Integer.valueOf(request.getParameter("age"));
			Long id = Long.parseLong(request.getParameter("id"));
			userService.updateUser(id, pwd, email, name, surname, nickname, birthday, age);
			response.sendRedirect(request.getContextPath()+"/user/list");
		} 
	}

}
