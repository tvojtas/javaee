package com.cgi.service;

import java.sql.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.cgi.api.AddressViewDto;
import com.cgi.api.ContactViewDto;
import com.cgi.api.NewUserDto;
import com.cgi.api.UpdateUserDto;
import com.cgi.api.UserDetailedViewDto;
import com.cgi.api.UserMeetingsDto;
import com.cgi.api.UserSimpleViewDto;
import com.cgi.api.UsersPerCityDto;
import com.cgi.data.UserDao;

@Stateless
public class UserService {
	
	@EJB
	private UserDao userDao;
	
	public UserDetailedViewDto findById(Long id) {
		return userDao.findById(id);
	}
	public List<UserSimpleViewDto> findAll() {
		return userDao.findAll();
	}
	public List<ContactViewDto> findContactsById(Long id) {
		return userDao.findContactsById(id);
	}
	public List<AddressViewDto> findAddressessById(Long id) {
		return userDao.findCAddressesById(id);
	}
	public void createNewUser(String pwd, String email, String name, String surname, String nickname, Date birthday, Integer age) {
		NewUserDto user = new NewUserDto();
		user.setPwd(pwd);
		user.setNickname(nickname);
		user.setEmail(email);
		user.setName(name);
		user.setSurname(surname);
		user.setBirthday(birthday);
		user.setAge(age);
		userDao.createNewUser(user);
	}
	public void updateUser(Long id, String pwd, String email, String name, String surname, String nickname, Date birthday, Integer age) {
		UpdateUserDto user = new UpdateUserDto();
		user.setNickname(nickname);
		user.setEmail(email);
		user.setName(name);
		user.setSurname(surname);
		user.setBirthday(birthday);
		user.setAge(age);
		userDao.updateUser(user, id);
		
	}
	public List<UsersPerCityDto> usersPerCity() {
		return userDao.usersPerCity();
	}
	public UserMeetingsDto findMeetingsById(Long id) {
		UserMeetingsDto userMeetings = new UserMeetingsDto();
		userMeetings.setCount(userDao.userMeetingsCount(id));
		userMeetings.setNext(userDao.userNextMeeting(id));
		userMeetings.setMeetings(userDao.findMeetingsById(id));
		return userMeetings;
	}
	public void deleteUser(Long id) {
		userDao.deleteUser(id);
	}
}
