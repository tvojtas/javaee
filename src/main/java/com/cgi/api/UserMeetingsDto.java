package com.cgi.api;

import java.sql.Timestamp;
import java.util.List;

public class UserMeetingsDto {
	private Integer count;
	private Timestamp next;
	private List<MeetingViewDto> meetings;
	
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Timestamp getNext() {
		return next;
	}
	public void setNext(Timestamp next) {
		this.next = next;
	}
	public List<MeetingViewDto> getMeetings() {
		return meetings;
	}
	public void setMeetings(List<MeetingViewDto> meetings) {
		this.meetings = meetings;
	}
	
}
